<?php

use Tygh\Registry;

$addon = 'ecloud_example';

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($mode == 'manage') {
    $www = Registry::get('config.http_host');

    if (fn_allowed_for('MULTIVENDOR')) {
        $edition = 'MULTIVENDOR';
    } else {
        $companies = db_get_field('SELECT COUNT(company_id) FROM ?:companies');
        if ($companies == 1) {
            $edition = 'CS-Cart';
        } else {
            $edition = 'Ultimate';
        }
    }

    $response = Tygh\Http::get('https://ecom.cloud/en/?dispatch=ec_licenses.check&addon=' . $addon . '&www=' . $www . '&edition=' . $edition);
    $timestamp = db_get_field('SELECT timestamp FROM ?:logs ORDER BY timestamp DESC LIMIT 0, 1');
    db_query('DELETE FROM ?:logs WHERE timestamp = ?i', $timestamp);
    $result = json_decode($response, true);

    if (!empty($result['status']) && AREA == 'A') {
        if ($result['status'] == 'A' && !isset($result['days'])) {
            // will check updates
        } elseif (isset($result['days']) && $result['status'] == 'A') {

            $addon_name = db_get_field('SELECT name FROM ?:addon_descriptions WHERE addon = ?s AND lang_code = ?s', $addon, CART_LANGUAGE);
            fn_set_notification('W', __('warning'), __('ecom_cloud_is_demo_days', array('[days]' => $result['days'], '[addon_name]' => $addon_name, '[addon]' => $addon)));

        } elseif ($result['status'] == 'D') {

            db_query('UPDATE `?:addons` SET status = ?s WHERE addon = ?s', 'D', $addon);
            fn_clear_cache();
            $addon_name = db_get_field('SELECT name FROM ?:addon_descriptions WHERE addon = ?s AND lang_code = ?s', $addon, CART_LANGUAGE);
            fn_set_notification('E', __('error'), __('ecom_cloud_disabled', array('[addon]' => $addon, '[addon_name]' => $addon_name)));

        }
    }
}